public class Obsvoistva {
    private String Name;
    private double Price;
    private int Count;

    public Obsvoistva(String Name, double price, int count){
        this.Name = Name;
        this.Price = price;
        this.Count = count;
    }
    public String getName() {
        return Name;
    }
    public void setName(String name) {
        this.Name = name;
    }
    public Double getPrice() {
        return Price;
    }
    public void setPrice(double price) {
        this.Price = price;
    }
    public int getCount() {
        return Count;
    }
    public void setCount(int count) {
        this.Count = count;
    }
}