import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Drugs[] dr = new Drugs[10];
        Krujki[] kr = new Krujki[10];
        kr[0]= new Krujki("Красивая кружка", 1000, 66, "Синий");
        kr[1]= new Krujki("Пивная кружка", 105, 5, "Красный");
        kr[2]= new Krujki("Большая кружка", 30, 7, "Желтый");
        kr[3]= new Krujki("Чашка", 77, 4, "Красный");
        kr[4]= new Krujki("Бокал", 2500, 61, "Прозрачный");
        kr[5]= new Krujki("Маленькая кружечка", 777, 15, "Розовый");
        kr[6]= new Krujki("Рюмка", 90, 12, "Прозрачный");
        kr[7]= new Krujki("Кружечка", 170, 10, "Белый");
        kr[8]= new Krujki("Обычная кружка", 290, 2, "Белый");
        kr[9]= new Krujki("Рюмка дорогая", 8000, 12, "Прозрачный");
        dr[0]= new Drugs("Лекарство", 5000, 15, "Лекарство");
        dr[1]= new Drugs("Афлубин", 350, 17, "Гомеопатическое");
        dr[2]= new Drugs("Фенозепам", 1000, 68, "Снотворное");
        dr[3]= new Drugs("Мирамистин", 100, 100, "Обеззараживающе");
        dr[4]= new Drugs("Спирт этиловый", 200, 15, "Спирт");
        dr[5]= new Drugs("МЕЛАКСЕН", 650, 18, "Снотворное");
        dr[6]= new Drugs("ДОНОРМИЛ", 350, 99, "Снотворное");
        dr[7]= new Drugs("КОРВАЛОЛ", 150, 9, "Снотворное");
        dr[8]= new Drugs("НОВОПАССИТ", 700, 333, "Снотворное");
        dr[9]= new Drugs("ПЕРСЕНФОРТЕ", 750, 12, "Снотворное");
        System.out.println ("Раздел кружек: " + "\n");
        for (int i = 0; i < kr.length; i++) {
            System.out.println("Название: " + kr[i].getName() + " Цена: " + kr[i].getPrice() + " Количество: " + kr[i].getCount()+ " Цвет: " + kr[i].getCvet() + "\n");
        }
        System.out.println ("Раздел лекарств: " + "\n");
        for (int i = 0; i < dr.length; i++) {
            System.out.println("Название: " + dr[i].getName() + " Цена: " + dr[i].getPrice() + " Количество: " + dr[i].getCount()+ " Классификация: " + dr[i].getClassification()+ "\n");
        }
    }
}
